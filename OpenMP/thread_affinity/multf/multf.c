#include <stdio.h>
#include <omp.h>

#define NRA 2048	// Number of rows matrix A
#define NCA 2048	// Number of colums matrix A
#define NCB 2048	// Number of colums matrix B

static float a[NRA][NCA] __attribute__(( aligned(256) ));
static float b[NCA][NCB] __attribute__(( aligned(256) ));
static float c[NRA][NCB] __attribute__(( aligned(256) ));

int
main ( int argc, char **argv ) {

	double time;
	float tmp;

	int i, j, k;

	// Init A, B, C
#pragma omp parallel for private(j)
	for ( i = 0; i < NRA; i++ )
#pragma omp simd
		for ( j = 0; j < NCA; j++ )
			a[i][j] = 1.;

#pragma omp parallel for private(j)
	for ( i = 0; i < NCA; i++ )
#pragma omp simd
		for ( j = 0; j < NCB; j++ )
			b[i][j] = 1.;

#pragma omp parallel for private(j)
	for ( i = 0; i < NRA; i++ )
	#pragma omp simd
		for ( j = 0; j < NCB; j++ )
			c[i][j] = 0.0;

	time = 0.0 - omp_get_wtime();


#ifndef _SEQ_
#pragma omp parallel for private(tmp, j, k)
#endif
	for ( i = 0; i < NRA; i++ )
		for ( j = 0; j < NCA; j++ ) {

			tmp = 0.0;
#pragma omp simd reduction( +:tmp )
			for ( k = 0; k < NCA; k++ )
				tmp += a[i][k] * b[j][k];
			c[i][j] = tmp;

		}

	time += omp_get_wtime();

	printf( "Done\n" );

	printf( "c[0][0] = %f\n", c[0][0] );
	printf( "c[NRA-1][NCB-1] = %f\n", c[NRA-1][NCB-1] );
	printf( "Execution time = %g seconds\n", time );
	printf( "\n" );

	return 0;

}


