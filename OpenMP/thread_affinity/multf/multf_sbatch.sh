#!/bin/bash

#SBATCH -J ppa_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -p thinnodes
#SBATCH -c 12
#SBATCH -t 10:00

CODE="multf"
FLAGS="-fopenmp -O2 -march=native -Wall -pedantic -fopt-info-vec" 

gcc ${FLAGS} -o ${CODE}.{out,c} -D_SEQ_
echo "-- Sequential"
for j in 1 2 3; do
	./${CODE}.out ${SIZE}
	echo "-"
done

gcc ${FLAGS} -o ${CODE}.{out,c}
export OMP_NUM_THREADS=12
export OMP_PLACES=sockets

export OMP_PROC_BIND=master
echo "-- ${OMP_PROC_BIND} Policy"
for j in 1 2 3; do
	./${CODE}.out ${SIZE}
	echo "-"
done

export OMP_PROC_BIND=close
echo "-- ${OMP_PROC_BIND} Policy"
for j in 1 2 3; do
	./${CODE}.out ${SIZE}
	echo "-"
done
