#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int
main ( int argc, char **argv ) {

	int N;
	float *x, *y;
	float dot, time;

	if ( argc < 2 ) {
		fprintf( stderr, "Use: %s num_elem_vector\n", *argv );
		exit( EXIT_FAILURE );
	}

	N = atoi( *++argv );

	if (( x = (float *) malloc( N*sizeof( x ) )) == NULL )
		printf( "Error in malloc x[%d]\n", N );

	if (( y = (float *) malloc( N*sizeof( y ) )) == NULL )
		printf( "Error in malloc y[%d]\n", N );


#pragma omp parallel for simd
	for ( int i = 0; i < N; i++ ) {
		x[i] = ( N/2.0 - i );
		y[i] = 0.0001 * i;
	}

	dot = 0.f;
	time = 0.0 - omp_get_wtime();

#ifndef _SEQ_
#pragma omp parallel for schedule( static, 4 ) reduction( +: dot )
#endif
	for ( int i = 0; i < N; i++ )
		dot += x[i] * y[i];

	time += omp_get_wtime();

	printf( "Dot product = %g\n", dot );
	printf( "Execution time: %gs\n", time );

	return 0;
}


