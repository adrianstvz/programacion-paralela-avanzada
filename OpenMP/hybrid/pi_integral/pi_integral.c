/******
 * MPI + OpenMP: Pi Calculation through integral calculation
 * 
 * Adrian Estevez Barreiro
 *****/

#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>


#define PI25DT 3.141592653589793238462643
#define ROOT 0


int
main ( int argc, char **argv ) {

	int id, np, provided;
	int for_start, for_end;
	long n = -1;
	double local_pi, pi, h, sum, x;
	double time;

	MPI_Init_thread( &argc, &argv, MPI_THREAD_FUNNELED, &provided );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );


	if ( ( id == ROOT ) && ( argc > 1 ) )
		n = atol( *++argv );

	MPI_Bcast( &n, 1, MPI_LONG, ROOT, MPI_COMM_WORLD );

	if ( n < 0 ) {
		if ( id == ROOT )
			fprintf( stderr, "Uso: %s num_iteraciones\n", *argv );
		MPI_Finalize();
		exit( EXIT_FAILURE );
	} 

	int my_pos[2];
	int *positions;

	time = 0.0 - omp_get_wtime();


	if ( id == ROOT ) {

#pragma omp parallel default( shared ) 
{
		if ( omp_get_thread_num() == ROOT )
			printf( "%d threads for each node, total nodes %d\n",
					omp_get_num_threads(), np );

}

		int start, end = 0;
		positions = malloc( 2 * np * sizeof( positions ) );

		// Calculate start/end for each proccess 
		for ( int i = 0; i < np; i++ ) {
			start = end;
			end = start + ( n / np ) + ( ( n % np ) > i );
			positions[(2*i)] = start;
			positions[(2*i) + 1] = end;
		}

	}

	// Scatter start/end to each proccess
	MPI_Scatter( positions, 2, MPI_INT, my_pos, 2, MPI_INT, 0, MPI_COMM_WORLD );
	for_start = my_pos[0];
	for_end   = my_pos[1];

	// Calculation starts 
	h 	= 1.0 / (double) n;
	sum = 0.0f;

	// # -- openmp
#pragma omp parallel for private( x ) reduction( +: sum )
	for ( int i = for_start; i < for_end; i++ ) { 
		x = h * (( double ) i + 0.5f );
		sum += 4.0f / ( 1.0f + x*x );
	}

	local_pi = h*sum;
	// Calculation ends


	MPI_Reduce( &local_pi, &pi, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD );

	time += omp_get_wtime();

	if ( id == ROOT )
		printf( "%02d: The obtained Pi value is: %.16f, the error is: %.16f\nExecution Time: %g seconds\n",
				id, pi, fabs( pi - PI25DT ), time);

	MPI_Finalize();

	return 0;
}


