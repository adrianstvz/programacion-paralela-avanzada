/******
 * MPI + OpenMP: Dot product
 * 
 * Adrian Estevez Barreiro
 *****/

#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>


#define ROOT 0

typedef unsigned int uint;

// ------
int
main ( int argc, char **argv ) {

	int N = -1;
	int id, np, provided;
	int count, recvsize, *sendsize, *displs;
	double *x, *y, *local_x, *local_y;
	double dot, local_dot, time;

	MPI_Init_thread( &argc, &argv, MPI_THREAD_FUNNELED, &provided );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );


	if ( ( id == ROOT ) && ( argc > 1 ) )
		N = atoi( *++argv );

	MPI_Bcast( &N, 1, MPI_INT, ROOT, MPI_COMM_WORLD );

	if ( N < 0 ) {
		if ( id == ROOT )
			fprintf( stderr, "Uso: %s num_elem_vector\n", *argv );
		
		MPI_Finalize();
		exit( EXIT_FAILURE );
	}


	if ( id == ROOT ) {

#pragma omp parallel default( shared ) 
{
		if ( omp_get_thread_num() == ROOT )
			printf( "%d threads for each node, total nodes %d\n",
					omp_get_num_threads(), np );

}

		if (( x = (double *) malloc( N*sizeof( x ) )) == NULL )
			printf( "Error in malloc x[%d]\n", N );

		if (( y = (double *) malloc( N*sizeof( y ) )) == NULL )
			printf( "Error in malloc y[%d]\n", N );

		// Init x and y
#pragma omp parallel for simd
		for ( uint i = 0; i < N; i++ ) {
			x[i] = ( N/2.0 - i );
			y[i] = 0.0001 * i;
		}

		count = 0;
		displs = (int *) malloc( sizeof(* displs) * np );
		sendsize = (int *) malloc( sizeof(* sendsize) * np );

		for ( uint i = 0; i < np; i++ ) {
			displs[i] = count;
			sendsize[i] = ( N / np ) + (( i < (N % np)) ? 1 : 0 );
			count += sendsize[i];
		}

	}

	time = 0.0 - omp_get_wtime();

	// Scatter start/end to each proccess
	MPI_Scatter( sendsize, 1, MPI_INT, &recvsize, 1, MPI_INT, 0, MPI_COMM_WORLD );

	if (( local_x = (double *) malloc( sizeof( local_x ) * recvsize )) == NULL )
		printf( "Error in malloc local_x[%d]\n", N );

	if (( local_y = (double *) malloc( sizeof( local_y ) * recvsize )) == NULL )
		printf( "Error in malloc local_y[%d]\n", N );

	// Scatter x and y
	MPI_Scatterv( x, sendsize, displs, MPI_DOUBLE, local_x, recvsize, MPI_DOUBLE, ROOT, MPI_COMM_WORLD );
	MPI_Scatterv( y, sendsize, displs, MPI_DOUBLE, local_y, recvsize, MPI_DOUBLE, ROOT, MPI_COMM_WORLD );


	// Calculation starts
	local_dot = 0.f;

	// # -- openmp
#pragma omp parallel for reduction( +: local_dot )
	for ( uint i = 0; i < recvsize; i++ )
		local_dot += local_x[i] * local_y[i];

	MPI_Reduce( &local_dot, &dot, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD );

	time += omp_get_wtime();

	if ( id == ROOT ) {
		printf( "Dot product = %g\n", dot );
		printf( "Execution time: %gs\n", time );
	}

	MPI_Finalize();


	return 0;

}


