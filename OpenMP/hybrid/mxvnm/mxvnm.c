/******
 * MPI + OpenMP: matrix vector product
 * 
 * Adrian Estevez Barreiro
 *****/
#include <math.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define ROOT 0

int
main ( int argc, char **argv ) {

	int id, np, provided;
	int N, M, size, nlines;

	int *displs, *sendsize;
	float **A, *Avector, *pmatrix, *x;
	double *y, *v_results;

	MPI_Init_thread( &argc, &argv, MPI_THREAD_FUNNELED, &provided );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );


	if ( argc < 3 ) {
		if ( id == ROOT ) 
			fprintf( stderr, "Use: %s N M", *argv );
		MPI_Finalize();
		exit( EXIT_FAILURE );
	}

	if ( id == ROOT ) {
		N = atoi( *++argv );
		M = atoi( *++argv );
	}

	MPI_Bcast( &N, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
	MPI_Bcast( &M, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
	

	if (( x = (float *) malloc( M * sizeof(x) )) == NULL )
		printf( "Error in malloc x[%d]\n", M );

	if ( id == ROOT ) {

#pragma omp parallel default( shared ) 
{
		if ( omp_get_thread_num() == ROOT )
			printf( "%d threads for each node, total nodes %d\n",
					omp_get_num_threads(), np );
}

		// Allocate master memory
		if (( y = (double *) malloc( N * sizeof( y ) )) == NULL )
			printf( "Error in malloc y[%d]\n", N );

		if (( Avector = (float *) malloc( N * M * sizeof( A ) )) == NULL ) 
			printf( "Error in malloc A[%d]\n", N*M );

		if (( A = (float **) malloc( N * sizeof( A ) )) == NULL ) 
			printf( "Error in malloc A[%d]\n", N*M );


		for ( int i = 0; i < N; i++ ) {
			x[i] = ( M / 2.0 - i );
			A[i] = Avector + i*M;
			for ( int j = 0; j < M; j++ )
				A[i][j] = ( 0.15*i - 0.1*j ) / N;
		}

		// Calculate sizes and displacements
		int count = 0;
		sendsize = (int *) malloc( sizeof(sendsize) * np );
		displs = (int *) malloc( sizeof(displs) * np );
		for ( int i = 0; i < np; i++ ) {
			displs[i] = count;
			sendsize[i] = M * (( N / np ) + (( i < ( N % np )) ? 1 : 0 )) ;
			count += sendsize[i];
		}
	}


	double time = 0.0 - MPI_Wtime();

	// Broadcast x
	MPI_Bcast( x, M, MPI_FLOAT, ROOT, MPI_COMM_WORLD );

	// Send each proccess its own nlines
	MPI_Scatter( sendsize, 1, MPI_INT, &size, 1, MPI_INT, ROOT, MPI_COMM_WORLD );

	nlines = size / M;

	// Alloc memory for private matrix and results vector
	if (( pmatrix = (float *) malloc( size * sizeof(pmatrix) )) == NULL )
		printf("Error in malloc pmatrix[%d]\n", N);

	if (( v_results = (double *) malloc( nlines * sizeof(v_results) )) == NULL )
		printf("Error in malloc v_results[%d]\n", N);

	// Distribute matrix among nodes
	MPI_Scatterv( Avector, sendsize, displs, MPI_FLOAT, pmatrix, size, MPI_FLOAT, ROOT, MPI_COMM_WORLD );

	double temp;
	for ( int i = 0; i < nlines; i++ ) {
		temp = 0.0;
#pragma omp parallel for simd reduction( +: temp )
		for ( int j = 0; j < M; j++ )
			temp += pmatrix[i*M + j] * x[j];
		v_results[i] = temp;
	}

	if ( id == ROOT ) {
		for ( int i = 0; i < np; i++ ) {
			displs[i] /= M;
			sendsize[i] /= M;
		}
	}

	MPI_Gatherv( v_results, nlines, MPI_DOUBLE, y, sendsize, displs, MPI_DOUBLE, ROOT, MPI_COMM_WORLD );


	time += MPI_Wtime();

	if ( id == ROOT ) {

		double result = 0.0;
		for ( int i = 0; i < N; i++ ) {
			result += y[i];
		}

		printf("Sum result is: %lf\n", result );
		printf("Time: %lf\n", time);

	}


	MPI_Finalize();

	return 0;
}


