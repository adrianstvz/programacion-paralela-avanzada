#!/bin/bash

#SBATCH -J ppa_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -p thinnodes,cola-corta
#SBATCH -n 16
#SBATCH --ntasks-per-node=16
#SBATCH -t 10:00

CODE="mxvnm"
FLAGS="-fopenmp -O2 -march=native -Wall -pedantic -fopt-info-vec"
N="10000"
M="20000"

module load gcc/6.4.0 openmpi/2.1.1

mpicc ${FLAGS} -o ./${CODE}.{out,c} 

for n in 1 2 4 8 16; do

	echo "Executing with ${n} processes and $(( 16 / ${n} )) threads"
	export OMP_NUM_THREADS=$(( 16 / ${n} ))
	for _ in 0 0 0; do
		srun --mpi=pmi2 -n ${n} --tasks-per-node=${n}  --cpus-per-task=$(( 16/${n} )) -- ./${CODE}.out ${N} ${M}
		echo
	done

	echo -ne "-----\n\n"

done

