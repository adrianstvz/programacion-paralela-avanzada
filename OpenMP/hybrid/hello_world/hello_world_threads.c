#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>


int
main ( int argc, char **argv ) {

	int id, np, namelen, iam, nt, provided;
	char nodename[MPI_MAX_PROCESSOR_NAME];

	MPI_Init_thread( &argc, &argv, MPI_THREAD_FUNNELED, &provided );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );
	MPI_Get_processor_name( nodename, &namelen );

#pragma omp parallel default( shared ) private( iam, nt ) 
	{
		nt = omp_get_num_threads();
		iam = omp_get_thread_num();

		printf( "I am thread %d from %d of process %d from %d in %s\n",
				iam, nt, id, np, nodename );
	}

	MPI_Finalize();

	return 0;

}


