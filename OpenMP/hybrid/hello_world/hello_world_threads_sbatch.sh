#!/bin/bash

#SBATCH -J ppa_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -p thinnodes,cola-corta
#SBATCH -n 16
#SBATCH --ntasks-per-node=16
##SBATCH -c 16
#SBATCH -t 10:00

CODE="hello_world_threads"

module load gcc/6.4.0 openmpi/2.1.1

mpicc -Wall -pedantic -o ./${CODE}.{out,c} -fopenmp

n=2

for n in 1 2 4 8 16; do

	echo "Excuting with ${n} processes and $(( 16 / ${n} )) threads"
	export OMP_NUM_THREADS=$(( 16 / ${n} ))
	echo "srun --mpi=pmi2 -n ${n} --tasks-per-node=${n} --cpus-per-task=$(( 16/${n} )) -- ./${CODE}.out"
	srun --mpi=pmi2 -n ${n} --tasks-per-node=${n}  -- ./${CODE}.out

	echo -ne "-----\n\n"

done
