#!/bin/bash

#SBATCH -J ppa_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -p thinnodes
#SBATCH -c 16
#SBATCH -t 10:00

CODE="multf"

NOVEC="-O2 -fno-tree-vectorize -fopenmp -Wall -pedantic -fopt-info-vec"
FLAGS="-fopenmp -O2 -march=native -Wall -pedantic -fopt-info-vec" 

for i in 1 2 4; do
	export OMP_NUM_THREADS=${i}
	echo "-- Executing mult with ${OMP_NUM_THREADS} threads"

	gcc ${NOVEC} -o ${CODE}.{out,c}
	echo "-- Not Vectorized"
	for j in 1 2 3; do
		./${CODE}.out 
		echo "-"
	done

	gcc ${FLAGS} -o ${CODE}.{out,c}
	echo "-- Vectorized"
	for j in 1 2 3; do
		./${CODE}.out 
		echo "-"
	done

done
