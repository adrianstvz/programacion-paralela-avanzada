#!/bin/bash

#SBATCH -J ppa_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -p thinnodes
#SBATCH -c 16
#SBATCH -t 10:00

CODE="saxpy"

FLAGS="-fopenmp -O2 -march=native -Wall -pedantic -fopt-info-vec" 


gcc ${FLAGS} -o ${CODE}.{out,c}
echo "-- Executing ${CODE} without parallelization"
for j in 1 2 3; do
	./${CODE}.out 
	echo "-"
done

for i in 1 2 4 8 16; do
	export OMP_NUM_THREADS=${i}
	echo "-- Executing ${CODE} with ${OMP_NUM_THREADS} threads"
	gcc ${FLAGS} -o ${CODE}2.{out,c}

	for j in 1 2 3; do
		./${CODE}2.out 
		echo "-"
	done

done
