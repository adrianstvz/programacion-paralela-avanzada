#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#define ROOT 0

int
main ( int argc, char **argv ) {

	int id, np;
	int N, M, size, nlines;

	int *displs, *sendsize;
	float **A, *Avector, *pmatrix, *x;
	double *y, *v_results;
	MPI_Request request = MPI_REQUEST_NULL;
	MPI_Request requestN = MPI_REQUEST_NULL;
	MPI_Request requestM = MPI_REQUEST_NULL;

	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );


	if ( argc < 3 ) {
		if ( id == ROOT ) 
			fprintf( stderr, "Use: %s N M", *argv );
		MPI_Finalize();
		exit( EXIT_FAILURE );
	}

	if ( id == ROOT ) {
		N = atoi( *++argv );
		M = atoi( *++argv );
	}

#ifdef _NONBLOCK_
	MPI_Ibcast( &N, 1, MPI_INT, ROOT, MPI_COMM_WORLD, &requestN );
	MPI_Ibcast( &M, 1, MPI_INT, ROOT, MPI_COMM_WORLD, &requestM );
#else
	MPI_Bcast( &N, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
	MPI_Bcast( &M, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
#endif
	

	if ( id != ROOT )
		MPI_Wait( &requestM, MPI_STATUS_IGNORE ); // Wait for bcast

	if (( x = (float *) malloc( M * sizeof(x) )) == NULL )
		printf( "Error in malloc x[%d]\n", M );

	if ( id == ROOT ) {

		// Allocate master memory
		if (( y = (double *) malloc( N * sizeof(y) )) == NULL )
			printf( "Error in malloc y[%d]\n", N );

		if (( Avector = (float *) malloc( N * M * sizeof(A) )) == NULL ) 
			printf( "Error in malloc A[%d]\n", N*M );

		if (( A = (float **) malloc( N * sizeof(A) )) == NULL ) 
			printf( "Error in malloc A[%d]\n", N*M );


		for ( int i = 0; i < N; i++ ) {
			x[i] = ( M / 2.0 - i );
			A[i] = Avector + i*M;
			for ( int j = 0; j < M; j++ )
				A[i][j] = ( 0.15*i - 0.1*j ) / N;
		}

		// Calculate sizes and displacements
		int count = 0;
		sendsize = (int *) malloc( sizeof(sendsize) * np );
		displs = (int *) malloc( sizeof(displs) * np );
		for ( int i = 0; i < np; i++ ) {
			displs[i] = count;
			sendsize[i] = M * (( N / np ) + (( i < ( N % np )) ? 1 : 0 )) ;
			count += sendsize[i];
		}
	}


	double time = 0.0 - MPI_Wtime();

	// Broadcast x
#ifdef _NONBLOCK_
	MPI_Ibcast( x, M, MPI_FLOAT, ROOT, MPI_COMM_WORLD, &request );
#else
	MPI_Bcast( x, M, MPI_FLOAT, ROOT, MPI_COMM_WORLD );
#endif


	// Send each proccess its own nlines
#ifdef _NONBLOCK_
	if ( id != ROOT ) {
		MPI_Wait( &requestN, MPI_STATUS_IGNORE ); // Wait for bcast
		MPI_Wait( &request, MPI_STATUS_IGNORE );
	}
	MPI_Iscatter( sendsize, 1, MPI_INT, &size, 1, MPI_INT, ROOT, MPI_COMM_WORLD, &request );
	MPI_Wait( &request, MPI_STATUS_IGNORE );
#else
	MPI_Scatter( sendsize, 1, MPI_INT, &size, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
#endif

	nlines = size / M;

	// Alloc memory for private matrix and results vector
	if (( pmatrix = (float *) malloc( size * sizeof(pmatrix) )) == NULL )
		printf("Error in malloc pmatrix[%d]\n", N);

	if (( v_results = (double *) malloc( nlines * sizeof(v_results) )) == NULL )
		printf("Error in malloc v_results[%d]\n", N);


	// Distribute matrix among nodes
#ifdef _NONBLOCK_
	MPI_Iscatterv( Avector, sendsize, displs, MPI_FLOAT, 
					pmatrix, size, MPI_FLOAT, ROOT, MPI_COMM_WORLD, &request );
	MPI_Wait( &request, MPI_STATUS_IGNORE );
#else
	MPI_Scatterv( Avector, sendsize, displs, MPI_FLOAT, pmatrix, size, MPI_FLOAT, ROOT, MPI_COMM_WORLD );
#endif


	double temp;
	for ( int i = 0; i < nlines; i++ ) {
		temp = 0.0;
		for ( int j = 0; j < M; j++ )
			temp += pmatrix[i*M + j] * x[j];
		v_results[i] = temp;
	}

	if ( id == ROOT ) {
		for ( int i = 0; i < np; i++ ) {
			displs[i] /= M;
			sendsize[i] /= M;
		}
	}

#ifdef _NONBLOCK_
	MPI_Igatherv( v_results, nlines, MPI_DOUBLE, y,
			sendsize, displs, MPI_DOUBLE, ROOT, MPI_COMM_WORLD, &request );
	MPI_Wait( &request, MPI_STATUS_IGNORE );
#else
	MPI_Gatherv( v_results, nlines, MPI_DOUBLE, y, sendsize, displs, MPI_DOUBLE, ROOT, MPI_COMM_WORLD );
#endif


	time += MPI_Wtime();

	if ( id == ROOT ) {

		double result = 0.0;
		for ( int i = 0; i < N; i++ ) {
			result += y[i];
		}

		printf("Sum result is: %lf\n", result );
		printf("Time: %lf seconds\n", time);

	}


	MPI_Finalize();

	return 0;

}


