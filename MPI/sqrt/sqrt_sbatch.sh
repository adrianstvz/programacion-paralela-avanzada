#!/bin/bash

#SBATCH -J ppa_job
#SBATCH -o %x-%j.log
#SBATCH -e %x-%j.err
#SBATCH -p thinnodes,cola-corta
#SBATCH -n 16
#SBATCH --ntasks-per-node=16
#SBATCH -t 10:00

CODE="sqrt"
FLAGS="-Wall -pedantic -lm"
SIZE="1000000"

module load gcc/6.4.0 openmpi/2.1.1

mpicc ${FLAGS} -o ./${CODE}.{out,c} -D_NONBLOCK_

echo "Execute Non Blocking"
for n in 1 2 4 8 16; do

	echo "Executing with ${n} processes"
	for steps in 1 2 4 8; do
		echo "Executing with ${steps} steps"
		for _ in 0 0 0; do
			srun --mpi=pmi2 -n ${n} --tasks-per-node=${n}  -- ./${CODE}.out ${SIZE} ${steps}
			echo
		done
	done
	echo -ne "-----\n\n"

done
