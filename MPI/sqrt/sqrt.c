#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define ROOT 0


float *create_rand_nums ( int ) ;
float my_test ( float *, int ) ;
void compute_sqrt ( float *, int, int ) ;

int
main ( int argc, char **argv ) {

	int id, np;
	int num_elements_per_proc, num_of_steps;
	double time;
	float *rand_nums, *sub_rand_nums, *result;

	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );

	
#ifdef _NONBLOCK_
	if ( argc < 3 ) {
		if ( id == ROOT ) 
			fprintf( stderr, "Use: %s SIZE NUMBER_OF_STEPS", *argv );
#else
	if ( argc < 2 ) {
		if ( id == ROOT ) 
			fprintf( stderr, "Use: %s SIZE", *argv );
#endif
		MPI_Finalize();
		exit( EXIT_FAILURE );
	}

	if ( id == ROOT ) {
		num_elements_per_proc = atoi( *++argv );
#ifdef _NONBLOCK_
		num_of_steps = atoi( *++argv );
#endif
	}


	MPI_Bcast( &num_elements_per_proc, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
#ifdef _NONBLOCK_
	MPI_Bcast( &num_of_steps, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
#endif

	if ( id == ROOT ) {
		if (( result = (float *) malloc( sizeof(* result) * np * num_elements_per_proc )) == NULL )
			printf( "Error in malloc result[%d]\n", np * num_elements_per_proc );

		rand_nums = create_rand_nums( num_elements_per_proc * np );
	}

	if (( sub_rand_nums = (float *) malloc( sizeof(* sub_rand_nums) * num_elements_per_proc )) == NULL )
		printf( "Error in malloc sub_rand_nums[%d]\n", num_elements_per_proc );

	MPI_Scatter( rand_nums, num_elements_per_proc, MPI_FLOAT, 
			sub_rand_nums, num_elements_per_proc, MPI_FLOAT, ROOT, MPI_COMM_WORLD );

	//printf( "%3d: elements scattered\n", id );

	time = 0.0f - MPI_Wtime();


#ifdef _NONBLOCK_
	
	int step_size = 0, pos = 0;
	MPI_Request request = MPI_REQUEST_NULL;
	
	int *displs, *recvsize;
	
	if ( id == ROOT ) {
		displs = (int *) malloc( sizeof(displs) * np );
		recvsize = (int *) malloc( sizeof(recvsize) * np );

		for ( int i = 0; i < np; i++ )
			displs[i] = i*num_elements_per_proc;

	}
	

	for ( int i = 0; i < num_of_steps; i++ ) {
		
		// Jumps to next position
		pos += step_size;

		step_size = ( num_elements_per_proc / num_of_steps ) + 
			(( i < ( num_elements_per_proc % num_of_steps )) ? 1 : 0 );

		// Computes current position
		compute_sqrt( &sub_rand_nums[pos], 0, step_size );

		if ( id == ROOT )
			for ( int i = 0; i < np; i++ )
				recvsize[i] = step_size;

		// Waits for previous msg
		if ( request != MPI_REQUEST_NULL )
			MPI_Wait( &request, MPI_STATUS_IGNORE );

		// Recolect current position
		MPI_Igatherv( &sub_rand_nums[pos], step_size, MPI_FLOAT, 
					&result[pos], recvsize, displs, MPI_FLOAT, ROOT, MPI_COMM_WORLD, &request );

	}

	// Waits for last msg
	MPI_Wait( &request, MPI_STATUS_IGNORE );

#else
	compute_sqrt( sub_rand_nums, 0, num_elements_per_proc );

	//printf( "%3d: elements computed\n", id );
	MPI_Gather( sub_rand_nums, num_elements_per_proc, MPI_FLOAT, 
				result, num_elements_per_proc, MPI_FLOAT, ROOT, MPI_COMM_WORLD );
#endif

	//printf( "%3d: elements gathered\n", id );

	time += MPI_Wtime();

	if ( id == ROOT ) {

		printf("Sum result is: %lf\n", my_test( result, np * num_elements_per_proc ));
		printf("Time: %lf seconds\n", time);

	}

	if ( id == ROOT ) {
		free( rand_nums );
		free( result );
	}
	free( sub_rand_nums );

	MPI_Finalize();

	return 0;
}

/// FUNCTIONS ---
float *
create_rand_nums ( int num_elements ) {

	float *rand_nums;

	if (( rand_nums	= (float *) malloc( sizeof(* rand_nums) * num_elements )) == NULL )
		printf( "Error in malloc rand_nums[%d]\n", num_elements );

	for ( int i = 0; i < num_elements; i++ ) 
		rand_nums[i] = 9.0;

	return rand_nums;
}

float
my_test ( float *array, int num_elements ) {

	float sum = 0.0f;
	for ( int i = 0; i < num_elements; i++ ) {
		sum += array[i];  
	}
	return sum;

}

void 
compute_sqrt ( float *array, int start, int end ) {

	for ( int i = start; i < end; i++ ) {
		array[i] = sqrt( array[i] );
	}

}
