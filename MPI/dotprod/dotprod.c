/******
 * MPI: Dot product
 * 
 * Adrian Estevez Barreiro
 *****/

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


#define ROOT 0

typedef unsigned int uint;

// ------
int
main ( int argc, char **argv ) {

	int N = -1;
	int id, np;
	int count, recvsize, *sendsize, *displs;
	double *x, *y, *local_x, *local_y;
	double dot, local_dot, time;
	MPI_Request request = MPI_REQUEST_NULL;
	MPI_Request request_x = MPI_REQUEST_NULL;
	MPI_Request request_y = MPI_REQUEST_NULL;

	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );


	if ( argc < 2 ) {
		if ( id == ROOT )
			fprintf( stderr, "Uso: %s num_iteraciones\n", *argv );
		MPI_Finalize();
		exit( EXIT_FAILURE );
	} 

	if ( id == ROOT ) 
		N = atoi( *++argv );

#ifdef _NONBLOCK_
	MPI_Ibcast( &N, 1, MPI_INT, ROOT, MPI_COMM_WORLD, &request );
#else
	MPI_Bcast( &N, 1, MPI_INT, ROOT, MPI_COMM_WORLD );
#endif

	if ( id == ROOT ) {

		if (( x = (double *) malloc( N*sizeof( x ) )) == NULL )
			printf( "Error in malloc x[%d]\n", N );

		if (( y = (double *) malloc( N*sizeof( y ) )) == NULL )
			printf( "Error in malloc y[%d]\n", N );

		// Init x and y
		for ( uint i = 0; i < N; i++ ) {
			x[i] = ( N/2.0 - i );
			y[i] = 0.0001 * i;
		}

		count = 0;
		displs = (int *) malloc( sizeof(* displs) * np );
		sendsize = (int *) malloc( sizeof(* sendsize) * np );

		for ( uint i = 0; i < np; i++ ) {
			displs[i] = count;
			sendsize[i] = ( N / np ) + (( i < (N % np)) ? 1 : 0 );
			count += sendsize[i];
		}

	}

	time = 0.0 - MPI_Wtime();

	// Scatter start/end to each proccess
#ifdef _NONBLOCK_
	if ( id != ROOT ) MPI_Wait(&request, MPI_STATUS_IGNORE); // Wait for bcast
	MPI_Iscatter( sendsize, 1, MPI_INT, &recvsize, 1, MPI_INT, 0, MPI_COMM_WORLD, &request );
	MPI_Wait(&request, MPI_STATUS_IGNORE);
#else
	MPI_Scatter( sendsize, 1, MPI_INT, &recvsize, 1, MPI_INT, 0, MPI_COMM_WORLD );
#endif

	if (( local_x = (double *) malloc( sizeof( local_x ) * recvsize )) == NULL )
		printf( "Error in malloc local_x[%d]\n", N );

	if (( local_y = (double *) malloc( sizeof( local_y ) * recvsize )) == NULL )
		printf( "Error in malloc local_y[%d]\n", N );

	// Scatter x and y
#ifdef _NONBLOCK_
	MPI_Iscatterv( x, sendsize, displs, MPI_DOUBLE, 
			local_x, recvsize, MPI_DOUBLE, ROOT, MPI_COMM_WORLD, &request_x );

	MPI_Iscatterv( y, sendsize, displs, MPI_DOUBLE, 
			local_y, recvsize, MPI_DOUBLE, ROOT, MPI_COMM_WORLD, &request_y );

	MPI_Wait(&request_x, MPI_STATUS_IGNORE);
	MPI_Wait(&request_y, MPI_STATUS_IGNORE);
#else
	MPI_Scatterv( x, sendsize, displs, MPI_DOUBLE, 
			local_x, recvsize, MPI_DOUBLE, ROOT, MPI_COMM_WORLD );

	MPI_Scatterv( y, sendsize, displs, MPI_DOUBLE, 
			local_y, recvsize, MPI_DOUBLE, ROOT, MPI_COMM_WORLD );
#endif

	// Calculation starts
	local_dot = 0.f;

	for ( uint i = 0; i < recvsize; i++ )
		local_dot += local_x[i] * local_y[i];

#ifdef _NONBLOCK_
	MPI_Ireduce( &local_dot, &dot, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD, &request );
	MPI_Wait(&request, MPI_STATUS_IGNORE);
#else
	MPI_Reduce( &local_dot, &dot, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD );
#endif

	time += MPI_Wtime();

	if ( id == ROOT ) {
		printf( "Dot product = %g\n", dot );
		printf( "Execution time: %gs\n", time );
	}

	MPI_Finalize();


	return 0;

}


