/******
 * MPI: Pi Calculation through integral calculation
 * 
 * Adrian Estevez Barreiro
 *****/

#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


#define PI25DT 3.141592653589793238462643
#define ROOT 0


int
main ( int argc, char **argv ) {

	int id, np;
	int for_start, for_end;
	long n;
	double local_pi, pi, h, sum, x;
	double time;
	MPI_Request request = MPI_REQUEST_NULL;

	MPI_Init( &argc, &argv );
	MPI_Comm_size( MPI_COMM_WORLD, &np );
	MPI_Comm_rank( MPI_COMM_WORLD, &id );


	if ( argc < 2 ) {
		if ( id == ROOT )
			fprintf( stderr, "Uso: %s num_iteraciones\n", *argv );
		MPI_Finalize();
		exit( EXIT_FAILURE );
	} 

	if ( ( id == ROOT ) && ( argc > 1 ) )
		n = atol( *++argv );

#ifdef _NONBLOCK_
	MPI_Ibcast( &n, 1, MPI_LONG, ROOT, MPI_COMM_WORLD, &request );
#else
	MPI_Bcast( &n, 1, MPI_LONG, ROOT, MPI_COMM_WORLD );
#endif


	int my_pos[2];
	int *positions;

	time = 0.0 - MPI_Wtime();

	if ( id == ROOT ) {

		int start, end = 0;
		positions = malloc( 2 * np * sizeof( positions ) );

		// Calculate start/end for each proccess 
		for ( int i = 0; i < np; i++ ) {
			start = end;
			end = start + ( n / np ) + ( ( n % np ) > i );
			positions[(2*i)] = start;
			positions[(2*i) + 1] = end;
		}

	}

	// Scatter start/end to each proccess
#ifdef _NONBLOCK_
	if ( id != ROOT ) MPI_Wait( &request, MPI_STATUS_IGNORE ); // Wait for bcast
	MPI_Iscatter( positions, 2, MPI_INT, my_pos, 2, MPI_INT, 0, MPI_COMM_WORLD, &request );
	MPI_Wait( &request, MPI_STATUS_IGNORE );
#else
	MPI_Scatter( positions, 2, MPI_INT, my_pos, 2, MPI_INT, 0, MPI_COMM_WORLD );
#endif

	for_start = my_pos[0];
	for_end   = my_pos[1];

	// Calculation starts 
	h 	= 1.0 / (double) n;
	sum = 0.0f;

	for ( int i = for_start; i < for_end; i++ ) { 
		x = h * (( double ) i + 0.5f );
		sum += 4.0f / ( 1.0f + x*x );
	}

	local_pi = h*sum;
	// Calculation ends


#ifdef _NONBLOCK_
	MPI_Ireduce( &local_pi, &pi, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD, &request );
	MPI_Wait( &request, MPI_STATUS_IGNORE );
#else
	MPI_Reduce( &local_pi, &pi, 1, MPI_DOUBLE, MPI_SUM, ROOT, MPI_COMM_WORLD );
#endif

	time += MPI_Wtime();

	if ( id == ROOT )
		printf( "%02d: The obtained Pi value is: %.16f, the error is: %.16f\nExecution Time: %g seconds\n",
				id, pi, fabs( pi - PI25DT ), time);

	MPI_Finalize();

	return 0;
}


